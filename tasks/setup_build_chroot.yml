- name: show which build chroot is being processed
  debug:
    msg: "Creating chroot for {{ item.distribution }}/{{ item.architecture }} (host_arch={{ dpkg_architecture }})"

- name: retrieve distribution-specific data
  ansible.builtin.set_fact:
    distro_data: "{{ lookup('distro_info', item.distribution) }}"

- name: compute various configuration values
  ansible.builtin.set_fact:
    chroot_codename: "{{ item.codename|default(distro_data.codename) }}"
    chroot_architecture: "{{ item.architecture }}"
    chroot_vendor: "{{ distro_data.vendor }}"
    chroot_extra_packages: "{{ sbuild_extra_packages + item.extra_packages|default([]) }}"
    chroot_mirror: "{{ item.mirror|default(distro_data.repository.mirror) }}"
    chroot_components: "{{ item.components|default(distro_data.repository.components) }}"
    chroot_extra_distributions: "{{ item.extra_distributions|default([]) }}"
    chroot_aliases: "{{ item.extra_aliases|default([]) }}"

- name: compute automatic aliases names
  ansible.builtin.set_fact:
    chroot_aliases: "{{ chroot_aliases + ['%s-%s-sbuild' % (alias, chroot_architecture)] }}"
  loop: "{{ distro_data.aliases }}"
  loop_control:
    loop_var: alias

- name: compute aliases names for extra distributions
  ansible.builtin.set_fact:
    chroot_aliases: "{{ chroot_aliases + ['%s-%s-sbuild' % (lookup('distro_info', alias).codename, chroot_architecture)] }}"
  loop: "{{ chroot_extra_distributions }}"
  loop_control:
    loop_var: alias

- name: compute chroot name
  ansible.builtin.set_fact:
    chroot_name: "{{ chroot_codename }}-{{ chroot_architecture }}-sbuild"

- name: add keyring package to extra packages
  ansible.builtin.set_fact:
    chroot_extra_packages: "{{ chroot_extra_packages + [ distro_data.repository.keyring_package ] }}"
  when: distro_data.repository.keyring_package != ''

- name: test existence of keyring file
  ansible.builtin.stat:
    path: "{{ distro_data.repository.keyring_file }}"
    get_checksum: no
  register: keyring_file


- name: handle build chroot creation transactionnally
  block:
    - name: create the build chroot
      ansible.builtin.command:
        cmd: >
          debootstrap --verbose --variant=buildd
          --arch={{ chroot_architecture }}
          --include={{ ",".join(chroot_extra_packages) }}
          --components={{ ",".join(chroot_components) }}
          {% if keyring_file.stat.exists %}--keyring={{ distro_data.repository.keyring_file }}{% endif %}
          --no-merged-usr
          {{ distro_data.debootstrap_options }}
          {{ item.debootstrap_options|default("") }}
          {{ chroot_codename }}
          {{ sbuild_chroot_dir }}/{{ chroot_name }}
          {{ chroot_mirror }}
          {% if item.debootstrap_script is defined -%}
              {{ item.debootstrap_script }}
          {%- else -%}
             {% if distro_data.debootstrap_script %}{{ distro_data.debootstrap_script }}{% endif %}
          {%- endif %}
        creates: "{{ sbuild_chroot_dir }}/{{ chroot_name }}/etc/apt/sources.list"

    - name: create schroot configuration file
      ansible.builtin.template:
        src: sbuild-schroot-template.j2
        dest: "/etc/schroot/chroot.d/{{ chroot_name }}"

    - name: overwrite apt's sources.list
      ansible.builtin.template:
        src: sbuild-apt-sources-list.j2
        dest: "{{ sbuild_chroot_dir }}/{{ chroot_name }}/etc/apt/sources.list"

    - name: create policy-rc.d file
      ansible.builtin.copy:
        dest: "{{ sbuild_chroot_dir }}/{{ chroot_name }}/usr/sbin/policy-rc.d"
        mode: 0755
        content: |
          #!/bin/sh
          echo "All runlevel operations denied by policy" >&2
          exit 101
      when: sbuild_create_policy_rc_d

    - name: add chroot to list of successfully built chroots
      ansible.builtin.set_fact:
        sbuild_managed_chroots: "{{ sbuild_managed_chroots + [{'name': chroot_name, 'path': sbuild_chroot_dir + '/' + chroot_name }] }}"

    - name: disable apt http pipelining
      # Freexian observed hash sum mismatches on stretch only. A typical cause
      # is broken pipelining. Thus disable it rather than try to backport a fix.
      # https://gitlab.com/freexian/sysadmin/ansible/-/issues/38
      when: chroot_codename == 'stretch'
      ansible.builtin.copy:
        dest: "{{ sbuild_chroot_dir }}/{{ chroot_name }}/etc/apt/apt.conf.d/02pipelining"
        mode: 0644
        owner: root
        group: root
        content: |
          Acquire::http::Pipeline-Depth 0;

  rescue:
    - name: ensure files from failed trial are gone
      ansible.builtin.file:
        path: "{{ path }}"
        state: absent
      loop:
        - "{{ sbuild_chroot_dir }}/{{ chroot_name }}"
        - "/etc/schroot/chroot.d/{{ chroot_name }}"
      loop_control:
        loop_var: path
      when: ansible_failed_task.name in ['create the build chroot','create schroot configuration file']
